import pandas as pd
import operator
import codecs
import re
import codecs
import pyspark
import sys
#sys.stdout = open('hdfs:///user/xh895/drug_abuse/print.txt', 'w')

sc = pyspark.SparkContext()
separator = re.compile('\W+') 

def levenshtein(s1, s2):
    '''
    return the levenshtein distance between str(s1) & str(s2)
    '''
    if len(s1) < len(s2):
        return levenshtein(s2, s1)
    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    return previous_row[-1]

def find_ngrams(input_list, n= 3):
    '''
    return a n-gram list that occured in the input_list
    '''
    return zip(*[input_list[i:] for i in range(n)])

def druglist(cell):
    try:
        return cell.lower().split(', ')
    except AttributeError:
        return ['']

def twid(lines):
    '''
    Given row of data, output drug abuse label, tweet text
    label: 0 = non-abuse, 1 = abuse
    '''
    for line in lines:
        line = line.split("\t")
        yield line[2], line[3][:-2]

tw = sc.textFile('hdfs:///user/xh895/drug_abuse/data.csv')
print tw.mapPartitions(twid).mapValues(lambda x: separator.split(x.lower())).mapValues(find_ngrams).take(2)
print 'labeled tw',tw.count(), 'labeled non-abuse', tw.filter(lambda x: x[0] == 0).count()
print 'labeled abuse',tw.filter(lambda x: x[0] == 1).count()
#map(find_ngrams, map(, tw)) #output n-gram corpus for each tweet

if __name__=='__main__':
    if len(sys.argv)<3:
        print "Usage: INPUT_PATH OUTPUT_PATH"
        sys.exit(-1)
    if len(sys.argv) == 3:
        print "Usage: stdout_path OUTPUT_PATH"
        #tw.saveAsTextFile("hdfs:///user/xh895/drug_abuse/" + sys.argv[2])
