
# Use spark to efficiently learn word2vec model & tf-idf weights
1. learn word2vec model
2. learn tf-idf weights 
3. compute a tf-idf weighted vector for each tweet, shaped [11100, n] (n = vector size)
4. From tweets trained word2vec mode, compute word2vec cector for labeled data.
	similar to step 1,2,3, compute a tf-idf weighted vector for labeled tweet, shaped [3700, n] (n = vector size)
5. dump data to a pickle file for later use