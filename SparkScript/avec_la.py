import pyspark
import pyspark.mllib.feature
import pandas as pd
import operator
import codecs
import re
import pyspark
import sys
import gensim
from sklearn.metrics.pairwise import cosine_similarity
from gensim import corpora
import nltk
from operator import add

def countone(rows):
    for row in rows:
        yield (row[0], row[1]), 1

def mp_content(rows):
    for row in rows:
        allcont =  row.split(',')
        for cont in allcont:
            if cont.startswith('"text"'):
                yield cont.split(':')[1][1:-1]

def mp_content_la(rows):
    for row in rows:
        allcont =  row[1].split(',')
        yield row[0], allcont

def word_tokenizer(rows):
    for row in rows:
        yield nltk.word_tokenize(row.lower())

def word_tokenizer2(rows):
    import re
    punc = re.compile('\W+')
    for row in rows:
        yield row[0], punc.split(row[1].lower())

def gettfidflist(rows):
    '''
    for each word, return a tf-idf weight
    '''
    for row in rows:
        tfidflist = []
        tl, doc = row
        for word in doc:
            tfidflist.append(tl[htfv.value.indexOf(word)])
        yield tfidflist

def twid(lines):
    for line in lines:
        line = line.split("\t")
        yield line[2], line[3]

def mapuselesskey(rows):
    '''
    for each row, map a useless key 0 as a tuple key
    '''
    for row in rows:
        yield [row]

def gettfidfvalues(rows):
    '''
    for joined dataset, 
    select tfidf values
    '''
    for row in rows:
        vector, tfidfraw = row[1]
        yield row.values
        
def getvector(rows):
    '''
    for each sentence, return a matrix of [number of words, vector size]
    '''
    import numpy as np
    for row in rows:
        vectorlist = []
        for word in row:
            vectorlist.append(vectors.value.get(word, [0]*200)) #vectors should be a broadcast object
        yield vectorlist
        
def getaverage(rows):
    '''
    for each sentence, get a tf-idf weighted average vector
    '''
    import numpy as np
    for row in rows:
        vec = row[0]
        tfidfv = row[1]
        yield np.dot(tfidfv, vec) / len(tfidfv)
                           
#get word vectors                                                                                                                                                     
def gettfidflist2(rows):
    '''                                                                                                                                                               
    for each word, return a tf-idf weight                                                                                                                             
    '''
    for row in rows:
        tfidflist = []
        tl, [ind, doc] = row
        for word in doc:
            tfidflist.append(tl[htfv.value.indexOf(word)])
        yield ind, tfidflist

def getaverage2(rows):
    '''                                                                                                                                                               
    for each sentence, get a tf-idf weighted average vector                                                                                                           
    '''
    import numpy as np
    for row in rows:
        vec = row[0]
        tfidfv = row[1][1]
        ind = row[1][0]
        yield ind,  np.dot(tfidfv, vec) / len(tfidfv)
     
if __name__ == '__main__':     
    
    sc = pyspark.SparkContext()
    data_path = 'hdfs:///user/xh895/tw/tw/twitter_drug_2017_01_01.jsonl'
    # load in tweets 
    twall = sc.textFile(data_path)
    doc = twall.mapPartitions(mp_content).mapPartitions(word_tokenizer) #select tweet txt content

    #get tf-idf
    htf = pyspark.mllib.feature.HashingTF(200)
    tf = htf.transform(doc) 
    tf.cache()
    idf = pyspark.mllib.feature.IDF(200).fit(tf)
    tfidf = idf.transform(tf)

    #get word vectors
    tfidflist = tfidf.zip(doc).\
                mapPartitions(gettfidflist)
    vectorlist = doc.mapPartitions(getvector)
    averagevector = vectorlist.zip(tfidflist).\
                                        mapPartitions(getaverage)

    # get word2vec vectors
    model = pyspark.mllib.feature.Word2Vec().setVectorSize(200).setSeed(42).fit(doc)
    vectors = model.getVectors()
    s = dict(zip(vectors.keys(), map(lambda x: list(x), vectors.values())))
    vectors = sc.broadcast(s)

    htfv = sc.broadcast(htf)
    lapath = 'hdfs:///user/xh895/tw/data.csv'
    ladoc = sc.textFile(lapath).mapPartitions(twid).mapPartitions(word_tokenizer2)

    htf = pyspark.mllib.feature.HashingTF(200)
    latf = htf.transform(ladoc.map(lambda x: x[1]))
    latf.cache()
    laidf = pyspark.mllib.feature.IDF(200).fit(latf)
    latfidf = laidf.transform(latf)

    latfidflist = latfidf.zip(ladoc).mapPartitions(gettfidflist2)
    lavectorlist = ladoc.map(lambda x: x[1]).mapPartitions(getvector)
    laaveragevector = lavectorlist.zip(latfidflist).mapPartitions(getaverage2)

    laaveragevector.map(lambda x: (x[0], x[1].tolist())).saveAsTextFile('hdfs:///user/xh895/vec/labeled200')
