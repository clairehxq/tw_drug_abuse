import pyspark
import pyspark.mllib.feature
import pandas as pd
import operator
import codecs
import re
import pyspark
import sys
import gensim
from sklearn.metrics.pairwise import cosine_similarity
from gensim import corpora
import nltk
from operator import add

sc = pyspark.SparkContext()
def countone(rows):
    for row in rows:
        yield (row[0], row[1]), 1

def mp_content(rows):
    for row in rows:
        allcont =  row.split(',')
        for cont in allcont:
            if cont.startswith('"text"'):
                yield cont.split(':')[1][1:-1]

def mp_content_la(rows):
    for row in rows:
        allcont =  row[1].split(',')
        yield row[0], allcont

def word_tokenizer(rows):
    for row in rows:
        yield nltk.word_tokenize(row.lower())

def word_tokenizer2(rows):
    import re
    punc = re.compile('\W+')
    for row in rows:
        yield row[0], punc.split(row[1].lower())

def gettfidflist(rows):
    '''
    for each word, return a tf-idf weight
    '''
    for row in rows:
        tfidflist = []
        tl, doc = row
        for word in doc:
            tfidflist.append(tl[htfv.value.indexOf(word)])
        yield tfidflist

def mapuselesskey(rows):
    '''
    for each row, map a useless key 0 as a tuple key
    '''
    for row in rows:
        yield [row]

def gettfidfvalues(rows):
    '''
    for joined dataset, 
    select tfidf values
    '''
    for row in rows:
        vector, tfidfraw = row[1]
        yield row.values
        
def getvector(rows):
    '''
    for each sentence, return a matrix of [number of words, vector size]
    '''
    import numpy as np
    for row in rows:
        vectorlist = []
        for word in row[1]:
            vectorlist.append(vectors.value.get(word, [0]*200)) #vectors should be a broadcast object
        yield row[0], vectorlist
        
def getaverage(rows):
    '''
    for each sentence, get a tf-idf weighted average vector
    '''
    import numpy as np
    for row in rows:
        vec = row[0]
        tfidfv = row[1]
        yield np.dot(tfidfv, vec) / len(tfidfv)

def twid(lines):
    for line in lines:
        line = line.split("\t")
        yield line[2], line[3]
                                                                                                                             
def gettfidflist2(rows):
    '''                                                                                                                                                    for each word, return a tf-idf weight                                                                                                                  '''
    for row in rows:
        tfidflist = []
        tl, [ind, doc] = row
        for word in doc:
            tfidflist.append(tl[htfv.value.indexOf(word)])
        yield ind, tfidflist

def getaverage2(rows):
    '''                                                                                                                                                    for each sentence, get a tf-idf weighted average vector                                                                                                '''
    import numpy as np
    for row in rows:
        vec = row[0]
        tfidfv = row[1][1]
        ind = row[1][0]
        yield ind,  np.dot(tfidfv, vec) / len(tfidfv)

if __name__ == '__main__':     
    data_path = 'hdfs:///user/xh895/tw/tw/twitter_drug_2017_01_01.jsonl'
    twall = sc.textFile(data_path)
    doc = twall.mapPartitions(mp_content).mapPartitions(word_tokenizer) #select tweet txt content
    # train word2vec models
    model = pyspark.mllib.feature.Word2Vec().setVectorSize(200).setSeed(42).fit(doc)
    vectors = model.getVectors()
    s = dict(zip(vectors.keys(), map(lambda x: list(x), vectors.values())))
    vectors = sc.broadcast(s)
    #get word vectors
    vectorlist = doc.mapPartitions(getvector)
    #get labeled data
    lapath = 'hdfs:///user/xh895/tw/data.csv'
    ladoc = sc.textFile(lapath).mapPartitions(twid).mapPartitions(word_tokenizer2)
    lavectorlist = ladoc.mapPartitions(getvector)
    lavectorlist.saveAsTextFile('hdfs:///user/xh895/vec/vla200')
