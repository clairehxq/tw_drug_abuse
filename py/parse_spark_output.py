import numpy as np
import pickle
import ast
#labeled data
with open('../SparkOutput/labeled1/part-00000', 'r') as f:
    la = f.readlines()
    la0 = map(lambda x: ast.literal_eval(x), la)
with open('../SparkOutput/labeled1/part-00001', 'r') as f:
    la = f.readlines()
    la1 = map(lambda x: ast.literal_eval(x), la)
la = la0+la1

#save some labeled data for testing
trainid = np.random.randint(0,len(la),size = int(len(la)*0.7))
latrain = [la[i] for i in trainid]
latest = [la[i] for i in (set(xrange(len(la))) - set(trainid))]

#unlabeled data
with open('../SparkOutput/spout4/part-00000', 'r') as f:
    un = f.readlines()
    un0 = map(lambda x: ast.literal_eval(x), un)   
with open('../SparkOutput/spout4/part-00001', 'r') as f:
    un = f.readlines()
    un1 = map(lambda x: ast.literal_eval(x), un)
with open('../SparkOutput/spout4/part-00002', 'r') as f:
    un = f.readlines()
    un2 = map(lambda x: ast.literal_eval(x), un)
un = un0+un1+un2

data = un+latrain
x = np.array(map(lambda x: x[1], data))
y = map(lambda x: int(x[0]), data)

#prepare test data
xte = map(lambda x: x[1], latest)
yte = map(lambda x: x[0], latest)

if __name__ == '__main__':
    # dump xtrain, ytrain, xtest, ytest as pickle file in data
    filenames = {'xtrf.pkl': x, 'ytrf':y, 'xtef':xte, 'ytef':yte}
    for f in filenames.keys():
        obj = open('../data/'+f, 'wb')
        pickle.dump(filenames[f], obj)
        obj.close()