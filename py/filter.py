import sys
import pyspark

def filterTweets(_, part):
    import json
    import re
    separator = re.compile('\W+')    
    allTerms = terms.value
    
    for line in part:
        record = json.loads(line)
        words = separator.split(record['text'].lower())
        for i in xrange(len(words)):
            w = [words[i]]
            if i+1<len(words):
                w.append(words[i] + ' ' + words[i+1])
            if len(allTerms.intersection(w))>0:
                yield line
                break

if __name__=='__main__':
    if len(sys.argv)<3:
        print "Usage: INPUT_PATH OUTPUT_PATH"
        sys.exit(-1)

    sc = pyspark.SparkContext()

    terms_illegal = set(sc.textFile('/user/hvo/terms/drug_illegal.csv').collect())
    terms_sched_2 = set(sc.textFile('/user/hvo/terms/drug_schedule_2.csv').collect())
    
    terms = sc.broadcast(terms_illegal | terms_sched_2)

    tc = sc.textFile(sys.argv[1]) \
        .mapPartitionsWithIndex(filterTweets) \
        .saveAsTextFile(sys.argv[2])
